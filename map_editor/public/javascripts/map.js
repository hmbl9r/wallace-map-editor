function map(tile) {
    this.tile = tile;
    this.map = {
        dimmensions: {
            width: 32,
            height: 32
        },
        tiles: []
    };

    var add = this.editor_place_tile_generator();
    var remove = this.editor_remove_tile_generator();
    var get = this.editor_get_tile_generator();

    var w = this.map.dimmensions.width,
        h = this.map.dimmensions.height;

    this.grid = new grid(32, 32, w, h, '#map', 'map-tile', {
        add: add,
        remove: remove,
        special: get
    });

    this.replace_tiles = true;
    var _this = this;

    $('#replace-tiles-checkbox').change(function() {
        _this.replace_tiles = this.checked;
    });

    $('#control-panel-map-export-button').click(this.export_map_generator());
    $('#import-map-dialog-file-browse').change(this.import_map_generator());

    for (var x = 0; x < w; ++x) {
        this.map.tiles.push([]);
        for (var y = 0; y < h; ++y) {
            this.map.tiles[x].push(-1);
        }
    }
}

map.prototype.editor_get_tile_generator = function() {
    var _this = this;
    return function(tile, x, y) {
        var i = _this.map.tiles[x][y];
        $('input[name="active-tile"][value="' + i + '"]').click();
    };
};

map.prototype.editor_place_tile_generator = function() {
    var _this = this;
    return function(tile, x, y) {
        if (_this.replace_tiles) {
            tile.css({
                backgroundImage: 'url(' + _this.tile.active_tile().image + ')'
            });
            _this.map.tiles[x][y] = parseInt(_this.tile.active_id, 10);
        } else {
            var new_tile = _this.tile.create_new_by_overlaying_to(_this.map.tiles[x][y]);
            tile.css({
                backgroundImage: 'url(' + new_tile.image + ')'
            });
            _this.map.tiles[x][y] = parseInt(new_tile.id, 10);
        }
    };
};

map.prototype.editor_remove_tile_generator = function() {
    var _this = this;
    return function(tile, x, y) {
        tile.css({
            background: 'none'
        });

        _this.map.tiles[x][y] = -1
    };
};

map.prototype.export_map_generator = function() {
    var t = this;
    return function() {
        $('<textarea>').text(JSON.stringify(t.map)).click(function() {
            $(this).select();
        }).appendTo($('#export-map-dialog > .export-dialog-data-wrapper').empty());
    };
};

map.prototype.import_map_generator = function() {
    var _this = this;
    return function(e) {
        var f = e.target.files[0];
        
        if (typeof(f) === 'undefined') {
            console.log('not json!', f.type);
        } else {
            var reader = new FileReader();

            reader.onload = function(e) {
                var data = JSON.parse(e.target.result);

                _this.map = data;

                _this.grid.draw({
                    get_tile_bg_img_func: function(x, y) {
                        var i = _this.map.tiles[x][y];
                        if (i < 0) {
                            return 'none';
                        } else {
                            var tile = _this.tile.tile_types[i];
                            return 'url(' + tile.image + ')';
                        }
                    }
                });
            };

            reader.readAsText(f, 'utf-8');
        }
    };
}