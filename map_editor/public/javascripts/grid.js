function grid(tw, th, gw, gh, grid_div_id, tile_class, callbacks) {
	this.tw = tw;
	this.th = th;
	this.gw = gw;
	this.gh = gh;
	this.grid_div = $(grid_div_id);
	this.tile_class = tile_class;

	this.add_callback = this.checked_func(callbacks.add);
	this.remove_callback = this.checked_func(callbacks.remove);
	this.special_callback = this.checked_func(callbacks.special);

	this.draw();
}

grid.prototype.checked_func = function(func) {
	if (typeof(func) === 'undefined') {
		return function() {};
	} else {
		return func;
	}
}

grid.prototype.draw = function(options) {
	if (typeof(options) === 'undefined') {
		options = {};
	}

	this.grid_div.empty();
	this.grid_div.addClass('editable-grid');
	this.grid_div.css({
		width: this.tw * this.gw,
		height: this.th * this.gh
	});
	this.grid_div.contextmenu(function() {
		return false;
	});

	var tile, bg_img;
	var table = $('<table>');

	var val = 0;

	for (var y = 0; y < this.gh; ++y) {
		var row = $('<tr>');
		for (var x = 0; x < this.gw; ++x) {
			if (typeof(options.get_tile_bg_color_func) === 'undefined') {
				tile = 'transparent';
			} else {
				tile = options.get_tile_bg_color_func(x, y);
			}

			if (typeof(options.get_tile_bg_img_func) === 'undefined') {
				bg_img = 'none';
			} else {
				bg_img = options.get_tile_bg_img_func(x, y);
			}

			$('<td>', {
				class: [this.tile_class, 'editable-grid-tile'].join(' ')
			}).css({
				width: this.tw,
				height: this.th,
				backgroundColor: tile,
				backgroundImage: bg_img
			}).appendTo(row);
		}

		row.appendTo(table);
	}

	table.appendTo(this.grid_div);

	var lm, rm, mm;

	var _this = this;
	function callback_if(t) {
		t = $(t);
		if (lm) {
			_this.add_callback(t, t.index(), t.parent().index());
		} else if (rm) {
			_this.remove_callback(t, t.index(), t.parent().index());
		} else if (mm) {
			_this.special_callback(t, t.index(), t.parent().index());
		}
	}

	function reset_m() {
		lm = false;
		mm = false;
		rm = false;
	}

	$('.' + this.tile_class).mousedown(function(e) {
    lm = e.which === 1;
    mm = e.which === 2;
    rm = e.which === 3;

    callback_if($(this));
  }).mouseenter(function() {
  	var _this = $(this);

  	_this.addClass('editable-grid-tile-hover');
  	callback_if(_this);
  }).mouseleave(function() {
  	$(this).removeClass('editable-grid-tile-hover');
  }).mouseup(reset_m);

  $('body').mouseup(reset_m).mouseleave(reset_m);
};