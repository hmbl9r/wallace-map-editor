function tile() {
    this.tile_types = {};
    this.used_names = {};
    this.active_id = '-1';
    this.current_id = 0;

    $('[rel="leanModal"]').leanModal();
    $('#import-dialog-file-button').change(this.import_tileset_generator());
    $('#delete-tile-button').click(this.delete_active_tile_generator());
    $('#control-panel-tiles-export').click(this.export_tileset_generator());
    $('#create-tile-button').click(this.setup_tile_editor_generator());

    this.load_saved_tiles_to_panel();
};

tile.prototype.setup_tile_editor_generator = function() {
    var t = this;
    return function() {
        var canvas = document.getElementById('create-tile-dialog-preview-canvas');
        canvas.width = 32;
        canvas.height = 32;
        context = canvas.getContext('2d');

        context.clearRect(0, 0, 32, 32);

        var tile_list = $('#create-tile-load-list').val('-1');

        tile_list.children().each(function(i, e) {
            e = $(e);
            if (e.val() !== '-1') {
                e.remove();
            }
        });

        for (var i in t.tile_types) {
            $('<option>', { value: i }).text(t.tile_types[i].name).appendTo(tile_list);
        }

        var current_color = 'black';

        function setup_color_picker(color) {
            $('#create-tile-dialog-color-picker').spectrum({
                color: color,
                showInput: true,
                preferredFormat: 'hex',
                flat: true,
                move: function(color) {
                    current_color = color.toRgbString();
                }
            });
        }

        setup_color_picker('black');

        function draw_on_canvas(clear, x, y) {
            if (clear) {
                context.clearRect(x, y, 1, 1);
            } else {
                context.fillStyle = current_color;
                context.fillRect(x, y, 1, 1);
            }
        }

        function add(tile, x, y) {
            tile.css({
                backgroundColor: current_color
            });
            draw_on_canvas(false, x, y);
        }

        function remove(tile, x, y) {
            tile.css({
                background: 'none'
            });
            draw_on_canvas(true, x, y);
        }

        function sample_color(tile) {
            current_color = tile.css('background-color');
            setup_color_picker(current_color);
        }

        var name = $('#create-tile-dialog-name').val('');
        name.removeClass('input-error');

        var _grid = new grid(16, 16, 32, 32, '#tile-being-edited', 'create-tile-pixel', {
            add: add,
            remove: remove,
            special: sample_color
        });
        
        $('#create-tile-save-button').unbind('click').click(function() {
            if (name.val() === '' || typeof(t.used_names[name.val()]) !== 'undefined') {
                name.addClass('input-error');
                return;
            }

            t.used_names[name.val()] = 1;

            name.removeClass('input-error');

            var the_new_tile = {
                name: name.val(),
                can_walk_on: true,
                image: canvas.toDataURL()
            };

            // TODO: fix bug where sometimes saved image is added to tiles twice
            var id = String(++t.current_id);
            t.tile_types[id] = the_new_tile;
            t.insert_new_tile(the_new_tile, id);
            t.save_tiles_in_panel();
        });

        tile_list.change(function() {
            var st = $(this);
            var the_tile = t.tile_types[st.val()];

            name.val(st.children('[value="' + st.val() + '"]').text());

            context.clearRect(0, 0, 32, 32);

            var img = new Image();
            img.src = the_tile.image;

            context.drawImage(img, 0, 0);

            function get_pixel_color(x, y) {
                var data = context.getImageData(x, y, 1, 1).data;

                var r = data[0],
                    g = data[1],
                    b = data[2],
                    a = data[3];

                return 'rgba(' + r + ',' + g + ',' + b + ',' + a + ')';
            }

            _grid.draw({ get_tile_bg_color_func: get_pixel_color });
        });

        $('#create-tile-dialog-clear').click(function() {
            $('.create-tile-pixel').css({ background: 'none' });
            context.clearRect(0, 0, 32, 32);
        });
    };
};

tile.prototype.delete_active_tile_generator = function() {
    var t = this;
    return function() {
        $('input[name="active-tile"]').filter(':checked').parent().remove();
        $('input[name="active-tile"]').first().click();

        delete t.tile_types[t.active_id];
        t.active_id = '-1';

        t.save_tiles_in_panel();
    };
};

tile.prototype.reset_attr_form = function() {
    $('#import-dialog-name-attr').val('').parent().removeClass('input-error');;
    $('#import-dialog-can-walk-attr').prop('checked', true);
};

tile.prototype.selected_new_active_tile = function() {
    var t = this;
    return function(e) {
        t.active_id = e.target.value;
    };
};

tile.prototype.insert_new_tile = function(tile, id) {
    var tile_row = $('<div>', { class: 'tile-row' });

    $('<input>', { type: 'radio', name: 'active-tile', value: id }).appendTo(tile_row).click(this.selected_new_active_tile());
    $('<img>', { src: tile.image }).css({ width: 16, height: 16 }).appendTo(tile_row);
    $('<p>').text(tile.name).appendTo(tile_row);

    tile_row.appendTo($('#control-panel-tiles-list'));

    $('.tile-row').tsort('p');
};

tile.prototype.save_tiles_in_panel = function() {
    localStorage.saved_tiles = JSON.stringify(this.tile_types);
};

tile.prototype.load_saved_tiles_to_panel = function() {
    try {
        this.tile_types = JSON.parse(localStorage.saved_tiles);
    } catch (e) {
        this.tile_types = {};
    }

    for (var i in this.tile_types) {
        if (typeof(this.used_names[this.tile_types[i].name]) !== 'undefined') {
            continue;
        }
        
        this.used_names[this.tile_types[i].name] = 1;
        this.insert_new_tile(this.tile_types[i], String(i));
        this.current_id = parseInt(i, 10);
    }

    $('input[name="active-tile"]').first().click();
};

tile.prototype.add_to_tile_panel_generator = function() {
    var t = this;
    return function() {
        var name = $('#import-dialog-name-attr'),
            can_walk_on = $('#import-dialog-can-walk-attr'),
            image = $('#import-dialog-preview-image');

        if (name.val() === '' || typeof(t.used_names[name.val()]) !== 'undefined') {
            name.parent().addClass('input-error');
            return;
        }

        t.used_names[name.val()] = 1;

        var tile = {
            name: name.val(),
            can_walk_on: can_walk_on.prop('checked'),
            image: image.attr('src')
        };

        var id = String(++t.current_id);
        t.tile_types[id] = tile;

        t.reset_attr_form();
        t.insert_new_tile(tile, id);
        t.save_tiles_in_panel();
    };
};

tile.prototype.cut_out_from_preview = function(c, context) {
    var tile_canvas = document.createElement('canvas');
    tile_canvas.width = 32;
    tile_canvas.height = 32;

    var tile_context = tile_canvas.getContext('2d');

    tile_context.putImageData(context.getImageData(c[0], c[1], 32, 32), 0, 0);

    return tile_canvas.toDataURL();
};

tile.prototype.import_tileset_generator = function() {
    var t = this;
    return function(e) {
        var f = e.target.files[0];

        var preview_wrapper = $('#import-dialog-preview-wrapper');
        var config_wrapper = $('#import-dialog-config-wrapper');

        $('#import-dialog-add-tile-button').click(t.add_to_tile_panel_generator(t));

        preview_wrapper.empty();
        
        if (typeof(f) === 'undefined' || !f.type.match('image.*')) {
            console.log('not an image!');
        } else {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#json-file-browse-wrapper').css('display', 'inline-block');
                var img = $('<img>', { src: e.target.result, usemap: '#preview-image-map' });

                preview_wrapper.css({ display: 'inline-block' });

                img.css({
                    border: '1px solid gray'
                }).appendTo(preview_wrapper);

                img.load(function() {
                    var w = img.width(),
                        h = img.height();

                    var canvas = document.createElement('canvas');
                    canvas.width = w;
                    canvas.height = h;

                    var context = canvas.getContext('2d');
                    var img_obj = new Image();

                    img_obj.src = img.attr('src');
                    context.drawImage(img_obj, 0, 0);


                    $('#import-dialog-json-file-button').change(t.import_tileset_metadata_generator(context));

                    function on_selected_tile(e) {
                        var c = e.target.coords.split(',');
                        $('#import-dialog-preview-image').attr('src', t.cut_out_from_preview(c, context));

                        config_wrapper.css({ display: 'inline-block' });
                    }

                    var map = $('<map>', { name: 'preview-image-map' });
                    map.appendTo(preview_wrapper);

                    for (var x = 0; x < w; x += 32) {
                        for (var y = 0; y < h; y += 32) {
                            var coords = x + ',' + y + ',' + (x + 32) + ',' + (y + 32);
                            $('<area>', { shape: 'rect', coords: coords }).click(on_selected_tile).appendTo(map);
                        }
                    }
                });

            };

            reader.readAsDataURL(f);
        }
    };
};

tile.prototype.create_new_by_overlaying_to = function(tile_id) {
    var bottom_tile = this.tile_types[tile_id];
    var top_tile = this.tile_types[this.active_id];

    // TODO: combine and shit.
};

tile.prototype.active_tile = function() {
    return this.tile_types[this.active_id];
};

tile.prototype.export_tileset_generator = function() {
    var t = this;
    return function() {
        var len = Object.keys(t.tile_types).length;

        var canvas = document.createElement('canvas');
        canvas.width = 32 * len;
        canvas.height = 32;

        var context = canvas.getContext('2d');

        var count = 0;
        var img, tmp_tile;
        var metadata = [];

        for (var i in t.tile_types) {
            tmp_tile = $.extend(true, {}, t.tile_types[i]);

            img = new Image();
            img.src = tmp_tile.image;

            delete tmp_tile.image;

            metadata.push(tmp_tile);

            context.drawImage(img, 32 * count, 0);
            ++count;
        }

        var url = canvas.toDataURL();

        $('<img>', { src: url }).appendTo($('#export-dialog-image > .export-dialog-data-wrapper').empty());
        $('<textarea>', { readonly: 'readonly' }).text(JSON.stringify(metadata)).click(function() {
            $(this).select();
        }).appendTo($('#export-dialog-json > .export-dialog-data-wrapper').empty());
    };
};

tile.prototype.import_tileset_metadata_generator = function(context) {
    var t = this;
    return function(e) {
        var f = e.target.files[0];
        
        if (typeof(f) === 'undefined') {
            console.log('not json!', f.type);
        } else {
            var reader = new FileReader();

            reader.onload = function(e) {
                var data = JSON.parse(e.target.result);

                for (var i in data) {
                    var the_tile = $.extend({ image: t.cut_out_from_preview([ 32 * i, 0 ], context) }, data[i]);

                    var id = String(++t.current_id);
                    t.tile_types[id] = the_tile;

                    t.insert_new_tile(the_tile);
                }

                t.save_tiles_in_panel();
            };

            reader.readAsText(f, 'utf-8');
        }
    };
};